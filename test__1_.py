# Import des fonctions à tester
from utils import lire_etudiants, generer_groupes


# Test unitaire pour la fonction lire_etudiants
def test_lire_etudiants(tmp_path):
    # Création d'un fichier CSV temporaire avec des données d'étudiants
    csv_data = """Nom,Prenom,Specialisation
    Baras,Hugo,DEVIA
    Cellier,Matthieu,CDA"""

    # Création du fichier CSV
    test_csv = tmp_path / "test.csv"
    test_csv.write_text(csv_data)

    # Appel de la fonction lire_etudiants avec le fichier CSV temporaire
    etudiants = lire_etudiants(test_csv)


# Test unitaire pour la fonction generer_groupes
def test_generer_groupes():
    # Création d'une liste d'étudiants simulée
    etudiants = [
        ("Hugo", "Baras"),
        ("Matthieu", "Cellier"),
        ("Alice", "Dupont"),
        ("Bob", "Martin"),
    ]

    # Appel de la fonction generer_groupes avec la liste d'étudiants simulée
    groupes = generer_groupes(etudiants)

    # Vérification du résultat
    assert len(groupes) == 2  # Vérifie si le nombre de groupes est correct

    for groupe in groupes:
        assert len(groupe) == 2  # Vérifie si chaque groupe contient 2 étudiants

        # Vérifie si les étudiants sont bien présents dans la liste initiale
        assert groupe[0] in etudiants
        assert groupe[1] in etudiants
