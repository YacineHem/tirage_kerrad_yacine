from utils import lire_etudiants, generer_groupes

def afficher_groupes(groupes):
    print("Groupe N;Prénom;NOM")
    groupe_num = 1
    for groupe in groupes:
        for nom, prenom in groupe:
            print(f"Groupe {groupe_num};{prenom};{nom}")
        groupe_num += 1


def main():
    etudiants = lire_etudiants("tirage_groupes\B3_Dev_promotion.csv")
    groupes = generer_groupes(etudiants)
    afficher_groupes(groupes)

if __name__ == '__main__':
    main()
