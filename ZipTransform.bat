@echo off
SETLOCAL

:: Obtenez le chemin du répertoire courant
SET "CURRENT_DIR=%~dp0"

:: Définissez le nom du fichier zip (vous pouvez le changer comme vous le souhaitez)
SET "ZIP_FILE=Archive.zip"

:: Chemin vers l'exécutable 7-Zip (changez-le si 7-Zip n'est pas dans votre PATH)
SET "SEVEN_ZIP_PATH=C:\Program Files\7-Zip\7z.exe"

:: Assurez-vous que 7-Zip est installé
IF NOT EXIST "%SEVEN_ZIP_PATH%" (
    echo 7-Zip n'est pas installe ou le chemin est incorrect.
    echo Assurez-vous que 7-Zip est installe et que le chemin dans ce script est correct.
    pause
    GOTO :EOF
)

:: Créez une archive zip du répertoire courant
"%SEVEN_ZIP_PATH%" a -tzip "%CURRENT_DIR%%ZIP_FILE%" "%CURRENT_DIR%*"

:: Fin du script
echo.
echo Archive creee : %ZIP_FILE%
pause
ENDLOCAL
