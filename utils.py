import csv
import random

def lire_etudiants(fichier_csv):
    with open(fichier_csv, newline='') as csvfile:
        lecteur = csv.reader(csvfile, delimiter=',')
        return [(ligne[2], ligne[1]) for ligne in lecteur]

def generer_groupes(etudiants):
    random.shuffle(etudiants)
    return [etudiants[i:i + 2] for i in range(0, len(etudiants), 2)]
