@echo off
SET IMAGE_NAME=httpd:latest

echo Téléchargement de l'image Apache...
docker pull %IMAGE_NAME%

echo Lancement du conteneur Apache...
docker run -p 80:80 -d %IMAGE_NAME%

echo Serveur Apache lancé avec succès.
